﻿namespace PostprocessorWPF.Backend
{
    public class MeshElement
    {
        public MeshElement()
        {
            MeshPoints = new Point3D[27];
            MeshIndex = new int[27];
        }

        /// <summary>
        ///     Узлы элемента
        /// </summary>
        public Point3D[] MeshPoints { get; set; }

        /// <summary>
        ///     Индексы узлов элемента
        /// </summary>
        public int[] MeshIndex { get; set; }
    }
}