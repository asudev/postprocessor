﻿namespace PostprocessorWPF.Backend
{
    public class CurrentElement
    {
        public CurrentElement()
        {
            Point3D = new Point3D[8];
        }

        public Point3D[] Point3D { get; set; }
    }
}