﻿using System.Collections.Generic;
using System.IO;
using Microsoft.Win32;

namespace PostprocessorWPF.Backend
{
    public class Generator
    {
        public Generator()
        {
            CurrentElements = new List<CurrentElement>();
            MeshElements = new List<MeshElement>();
            //var builder = new BuilderB();

            //GetMeshElements(builder.Points3D);
            //GetCurrentElements();
        }

        public static bool IsActive { get; set; } = false;
        public static List<MeshElement> MeshElements { get; set; }
        public static List<CurrentElement> CurrentElements { get; set; }

        public static void GetMeshElements(List<Point3D> points)
        {
            MeshElements = new List<MeshElement>();

            //var points = GetTestPoints();
            if (points.Count != 0)
            {
                var count = points.Count/27;
                var j = 0;
                var n = 0;
                for (var i = 1; i <= count; i++)
                {
                    var element = new MeshElement();

                    for (; j < 27*i; j++, n++)
                    {
                        element.MeshPoints[n] = points[j];
                    }
                    n = 0;
                    MeshElements.Add(element);
                }
            }
        }

        public static void GetCurrentElements(List<MeshElement> meshElements)
        {
            CurrentElements = new List<CurrentElement>();

            if (meshElements.Count != 0)
            {
                foreach (var meshElement in meshElements)
                {
                    for (int i = 0, j = 0; i < 8; i++, j++)
                    {
                        if (j == 2)
                            j++;
                        else if (j == 5)
                            j = 9;
                        else if (j == 11)
                            j++;

                        var curElement = new CurrentElement
                        {
                            Point3D =
                            {
                                [0] = meshElement.MeshPoints[j],
                                [1] = meshElement.MeshPoints[j + 1],
                                [2] = meshElement.MeshPoints[j + 3],
                                [3] = meshElement.MeshPoints[j + 4],
                                [4] = meshElement.MeshPoints[j + 9],
                                [5] = meshElement.MeshPoints[j + 10],
                                [6] = meshElement.MeshPoints[j + 12],
                                [7] = meshElement.MeshPoints[j + 13]
                            }
                        };

                        CurrentElements.Add(curElement);
                    }

                }
                IsActive = true;
            }
        }

        public List<Point3D> GetTestPoints()
        {
            var testPoints = new List<Point3D>
            {
                #region Element1

                //// Element1
                //// bottom 9
                //new Point3D(0, 0, 0),
                //new Point3D(0.5, 0.2, 0),
                //new Point3D(1, 0, 0),
                //new Point3D(0, 0.5, 0),
                //new Point3D(0.5, 0.5, 0),
                //new Point3D(1, 0.5, 0),
                //new Point3D(0, 1, 0),
                //new Point3D(0.5, 1, 0),
                //new Point3D(1, 1, 0),

                //// middle 9
                //new Point3D(0, 0, 0.5),
                //new Point3D(0.5, 0.2, 0.5),
                //new Point3D(1, 0, 0.5),
                //new Point3D(0, 0.5, 0.5),
                //new Point3D(0.5, 0.5, 0.5),
                //new Point3D(1, 0.5, 0.5),
                //new Point3D(0, 1, 0.5),
                //new Point3D(0.5, 1, 0.5),
                //new Point3D(1, 1, 0.5),

                //// top
                //new Point3D(0, 0, 1),
                //new Point3D(0.5, 0.3, 1),
                //new Point3D(1, 0, 1),
                //new Point3D(0, 0.5, 1),
                //new Point3D(0.5, 0.5, 1),
                //new Point3D(1, 0.5, 1),
                //new Point3D(0, 1, 1),
                //new Point3D(0.5, 1, 1),
                //new Point3D(1, 1, 1),
                #endregion

                #region Element2
                // Element2
                // bottom 9
                new Point3D(-1,-1,-1),
                new Point3D(0,-1,-1),
                new Point3D(1,-1,-1),
                new Point3D(-1,0,-1),
                new Point3D(0,0,-1),
                new Point3D(1,0,-1),
                new Point3D(-1,1,-1),
                new Point3D(0,1,-1),
                new Point3D(1,1,-1),

                // middle 9
                new Point3D(-1,-1,0),
                new Point3D(0,-1,0),
                new Point3D(1,-1,0),
                new Point3D(-1,0,0),
                new Point3D(0,0,0),
                new Point3D(1,0,0),
                new Point3D(-1,1,0),
                new Point3D(0,1,0),
                new Point3D(1,1,0),

                // top
                new Point3D(-1,-1,1),
                new Point3D(0,-1,1),
                new Point3D(1,-1,1),
                new Point3D(-1,0,1),
                new Point3D(0,0,1),
                new Point3D(1,0,1),
                new Point3D(-1,1,1),
                new Point3D(0,1,1),
                new Point3D(1,1,1),
                #endregion
            };

            return testPoints;
        }

        public void WriteTestPoints()
        {
            var folderDialog = new SaveFileDialog
            {
                Title = ";k;lk"
            };
            if (folderDialog.ShowDialog() != true) return;
            var pointsPath = folderDialog.FileName;

            using (
                var writer =
                    new BinaryWriter(File.Create(pointsPath + "TestPoints.dat", GetTestPoints().Count,
                        FileOptions.Asynchronous)))
            {
                foreach (var point in GetTestPoints())
                {
                    writer.Write(point.X);
                    writer.Write(point.Y);
                    writer.Write(point.Z);
                }
            }
        }
    }
}