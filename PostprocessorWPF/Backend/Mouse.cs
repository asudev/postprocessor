﻿using System.Windows;

namespace PostprocessorWPF.Backend
{
    public class Mouse
    {
        public double RotateSpeed { get; set; } = 0.004;
        public double ZoomSpeed { get; set; } = 0.005;
        public double MoveSpeed { get; set; } = 0.01;

        public Point Position { get; set; }
    }
}