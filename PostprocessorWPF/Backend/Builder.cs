﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using SharpGL;
using System.Threading.Tasks;
using System.Windows;

namespace PostprocessorWPF.Backend
{
    public class Builder
    {
        private List<Element> Elements { get; set; }
        
        private List<Point3D> Points { get; set; }

        public Bounds Bounds { get; }

        public bool IsInclude { get; set; }

        public List<int> MaterialsToDraw { get; set; }

        public Builder()
        {
            Bounds = new Bounds();
            MaterialsToDraw = new List<int>();
        }

        public Point3D CenterPoint3D
        {
            get
            {
                var xMax = Points.Max(p => p.X);
                var xMin = Points.Min(p => p.X);

                var yMax = Points.Max(p => p.Y);
                var yMin = Points.Min(p => p.Y);

                var zMax = Points.Max(p => p.Z);
                var zMin = Points.Min(p => p.Z);

                return new Point3D((xMin + xMax) / 2, (yMin + yMax) / 2, (zMin + zMax) / 2);
            }
        }

        public void ReadPoints(string path)
        {
            Points = new List<Point3D>();
            var regex = new Regex(@"[+-]?\d+(\.\d*)?");

            using (var reader = new StreamReader(path))
            {
                var line = reader.ReadLine();
                if (line == null)
                {
                    return;
                }

                var s = regex.Matches(line);

                var xCount = Convert.ToInt32(s[0].Value);
                var yCount = Convert.ToInt32(s[1].Value);
                var zCount = Convert.ToInt32(s[2].Value);

                for (var i = 0; i < zCount * yCount; i++)
                {
                    line = reader.ReadLine();
                    if (line == null)
                    {
                        continue;
                    }

                    s = regex.Matches(line);
                    for (var j = 0; j < xCount; j++)
                    {
                        var x = Convert.ToDouble(s[3 * j].Value, CultureInfo.InvariantCulture);
                        var y = Convert.ToDouble(s[3 * j + 1].Value, CultureInfo.InvariantCulture);
                        var z = Convert.ToDouble(s[3 * j + 2].Value, CultureInfo.InvariantCulture);

                        var point = new Point3D(x, y, z);
                        Points.Add(point);
                    }
                }
            }
        }

        public void ReadPoints2(string path)
        {
            Points = new List<Point3D>();
            var regex = new Regex(@"[+-]?\d+(\.\d*)?");

            using (var reader = new StreamReader(path))
            {
                var line = reader.ReadLine();
                if (line == null)
                {
                    return;
                }

                var s = regex.Matches(line);

                var count = Convert.ToInt32(s[0].Value);

                for (var i = 0; i < count; i++)
                {
                    line = reader.ReadLine();
                    if (line == null)
                    {
                        continue;
                    }

                    s = regex.Matches(line);

                    var x = Convert.ToDouble(s[0].Value, CultureInfo.InvariantCulture);
                    var y = Convert.ToDouble(s[1].Value, CultureInfo.InvariantCulture);
                    var z = Convert.ToDouble(s[2].Value, CultureInfo.InvariantCulture);

                    var point = new Point3D(x, y, z);
                    Points.Add(point);
                }
            }
        }

        public void ReadElements(string path)
        {
            Elements = new List<Element>();
            var regex = new Regex(@"\b[+-]?\d+(\.\d*)?\b");

            using (var reader = new StreamReader(path))
            {
                var line = reader.ReadLine();
                if (line == null)
                {
                    return;
                }

                var elementCount = Convert.ToInt32(line);

                for (var i = 0; i < elementCount; i++)
                {
                    line = reader.ReadLine();
                    if (line == null)
                    {
                        continue;
                    }

                    var element = new Element();
                    var s = regex.Matches(line);

                    element.Material = Convert.ToInt32(s[0].Value);

                    for (var j = 1; j < 9; j++)
                    {
                        element.Points[j - 1] =
                            Points[Convert.ToInt32(s[j].Value, CultureInfo.InvariantCulture) - 1];
                    }
                    Elements.Add(element);
                    AddMaterial(element.Material);
                }
            }
        }

        public void AddMaterial(int material)
        {
            if (!MaterialsToDraw.Contains(material) && material != 0)
            {
                MaterialsToDraw.Add(material);
            }
        }

        public bool IsFirst { get; set; } = false;
        Stopwatch watch = new Stopwatch();

        public void Draw(OpenGL gl)
        {
            if (IsFirst)
            {
#if DEBUG
                watch.Start();
#endif
            }

            if (Elements != null)
            {
                foreach (var element in Elements)
                {
                    if (IsInclude)
                    {
                        if (element.IsInBounds(Bounds))
                        {
                            if (MaterialsToDraw.Contains(element.Material))
                            {
                                element.Draw(gl);
                            }
                        }
                    }
                    else
                    {
                        if (!element.IsInBounds(Bounds))
                        {
                            if (MaterialsToDraw.Contains(element.Material))
                            {
                                element.Draw(gl);
                            }
                        }
                    }
                }
                if (IsFirst)
                {
#if DEBUG
                    watch.Stop();
                    MessageBox.Show(watch.ElapsedMilliseconds.ToString());

                    IsFirst = false;
#endif
                }
            }
        }

        public void SetMode(DrawMode mode)
        {
            if (Elements == null)
            {
                return;
            }
            foreach (var element in Elements)
            {
                element.Mode = mode;
            }
        }
    }
}