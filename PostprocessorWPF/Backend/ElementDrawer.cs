﻿using System.Diagnostics;
using System.Windows.Media;
using SharpGL;
using SharpGL.Enumerations;

namespace PostprocessorWPF.Backend
{
    public class ElementDrawer
    {
        private readonly bool IsFirst = true;

        /// <summary>
        ///     Цвет полигонов
        /// </summary>
        private Color elementsColor => Color.FromRgb(236, 240, 241);

        /// <summary>
        ///     Цвет линий
        /// </summary>
        private Color linessColor => Color.FromRgb(149, 165, 166);

        /// <summary>
        ///     Отрисовка одного элемента по заданым точкам.
        /// </summary>
        /// <param name="gl"></param>
        /// <param name="currentElement">выбранный объект</param>
        /// <param name="beginMode">выбор отрисовки элемента</param>
        private void DrawElement(OpenGL gl, CurrentElement currentElement, BeginMode beginMode)
        {
            if (beginMode != BeginMode.Polygon)
            {
                gl.Color(linessColor.ScR, linessColor.ScG, linessColor.ScB);
                gl.LineWidth(4);
            }
            else
            {
                gl.Color(elementsColor.ScR, elementsColor.ScG, elementsColor.ScB);
            }

            // Bottom.
            gl.Begin(beginMode);
            gl.Vertex(currentElement.Point3D[0].X, currentElement.Point3D[0].Y, currentElement.Point3D[0].Z);
            gl.Vertex(currentElement.Point3D[2].X, currentElement.Point3D[2].Y, currentElement.Point3D[2].Z);
            gl.Vertex(currentElement.Point3D[3].X, currentElement.Point3D[3].Y, currentElement.Point3D[3].Z);
            gl.Vertex(currentElement.Point3D[1].X, currentElement.Point3D[1].Y, currentElement.Point3D[1].Z);
            gl.End();

            // Left.
            gl.Begin(beginMode);
            gl.Vertex(currentElement.Point3D[0].X, currentElement.Point3D[0].Y, currentElement.Point3D[0].Z);
            gl.Vertex(currentElement.Point3D[4].X, currentElement.Point3D[4].Y, currentElement.Point3D[4].Z);
            gl.Vertex(currentElement.Point3D[6].X, currentElement.Point3D[6].Y, currentElement.Point3D[6].Z);
            gl.Vertex(currentElement.Point3D[2].X, currentElement.Point3D[2].Y, currentElement.Point3D[2].Z);
            gl.End();

            // Right.
            gl.Begin(beginMode);
            gl.Vertex(currentElement.Point3D[1].X, currentElement.Point3D[1].Y, currentElement.Point3D[1].Z);
            gl.Vertex(currentElement.Point3D[3].X, currentElement.Point3D[3].Y, currentElement.Point3D[3].Z);
            gl.Vertex(currentElement.Point3D[7].X, currentElement.Point3D[7].Y, currentElement.Point3D[7].Z);
            gl.Vertex(currentElement.Point3D[5].X, currentElement.Point3D[5].Y, currentElement.Point3D[5].Z);
            gl.End();

            // Top.
            gl.Begin(beginMode);
            gl.Vertex(currentElement.Point3D[4].X, currentElement.Point3D[4].Y, currentElement.Point3D[4].Z);
            gl.Vertex(currentElement.Point3D[5].X, currentElement.Point3D[5].Y, currentElement.Point3D[5].Z);
            gl.Vertex(currentElement.Point3D[7].X, currentElement.Point3D[7].Y, currentElement.Point3D[7].Z);
            gl.Vertex(currentElement.Point3D[6].X, currentElement.Point3D[6].Y, currentElement.Point3D[6].Z);
            gl.End();

            // Front.
            gl.Begin(beginMode);
            gl.Vertex(currentElement.Point3D[0].X, currentElement.Point3D[0].Y, currentElement.Point3D[0].Z);
            gl.Vertex(currentElement.Point3D[1].X, currentElement.Point3D[1].Y, currentElement.Point3D[1].Z);
            gl.Vertex(currentElement.Point3D[5].X, currentElement.Point3D[5].Y, currentElement.Point3D[5].Z);
            gl.Vertex(currentElement.Point3D[4].X, currentElement.Point3D[4].Y, currentElement.Point3D[4].Z);
            gl.End();

            // Back.
            gl.Begin(beginMode);
            gl.Vertex(currentElement.Point3D[3].X, currentElement.Point3D[3].Y, currentElement.Point3D[3].Z);
            gl.Vertex(currentElement.Point3D[2].X, currentElement.Point3D[2].Y, currentElement.Point3D[2].Z);
            gl.Vertex(currentElement.Point3D[6].X, currentElement.Point3D[6].Y, currentElement.Point3D[6].Z);
            gl.Vertex(currentElement.Point3D[7].X, currentElement.Point3D[7].Y, currentElement.Point3D[7].Z);
            gl.End();
        }

        /// <summary>
        /// Отрисовка всех элементов коллекции.
        /// </summary>
        /// <param name="gl"></param>
        public void DrawAll(OpenGL gl)
        {
            var watch = new Stopwatch();

            if (Generator.CurrentElements == null) return;

            if (Generator.CurrentElements.Count != 0 && Generator.IsActive)
            {
                if (IsFirst)
                {
#if DEBUG
                    watch.Start();
#endif
                }
                foreach (var currentElement in Generator.CurrentElements)
                {
                    DrawElement(gl, currentElement, BeginMode.Polygon);
                    DrawElement(gl, currentElement, BeginMode.LineLoop);
                }
                if (IsFirst)
                {
#if DEBUG
                    watch.Stop();
                    MessageBox.Show(watch.ElapsedMilliseconds.ToString());
                    IsFirst = false;
#endif
                }
            }
        }
    }
}