﻿namespace PostprocessorWPF.Backend
{
    public class Bounds
    {
        public Bounds()
        {
            Reset();
        }

        public double X1 { get; set; }

        public double X2 { get; set; }

        public double Y1 { get; set; }

        public double Y2 { get; set; }

        public double Z1 { get; set; }

        public double Z2 { get; set; }

        public void Reset()
        {
            X1 = Y1 = Z1 = -double.MaxValue;
            X2 = Y2 = Z2 = double.MaxValue;
        }
    }
}