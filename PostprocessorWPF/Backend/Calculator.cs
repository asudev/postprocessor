﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostprocessorWPF.Backend
{
    public class Calculator
    {
        private double sumX = 0;
        private double sumY = 0;
        private double sumZ = 0;
        Generator gen = new Generator();


        public Calculator()
        {
            
            ComputeSumms(gen.GetTestPoints());
        }


        public List<Point3D> ComputeSumms(List<Point3D> point)
        {
            var pointsList = new List<Point3D>();

            for (var l = 0; l < 27; l++)
            {
                sumX += Psi(point[l], l)*point[l].X;
                sumY += Psi(point[l], l)*point[l].Y;
                sumZ += Psi(point[l], l)*point[l].Z;
            }
            pointsList.Add(new Point3D(sumX, sumY, sumZ));

            return pointsList;
        }

        private double Phi(int index, double param)
        {
            switch (index)
            {
                case 0:
                    return (param * (param - 1)) / 2;
                case 1:
                    return (Math.Pow(-param, 2) + 1);
                case 2:
                    return (param * (param + 1)) / 2;
                default:
                    throw new Exception("problem with calculate", null);
            }
        }

        private double Psi(Point3D point, int l)
        {
            var i = l%3;
            var j = (l/3)%3;
            var k = l/9;

            var phi = Phi(i, point.X)*Phi(j, point.Y)*Phi(k, point.Z);

            return phi;
        }

    }
}
