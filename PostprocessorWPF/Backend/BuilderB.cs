﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PostprocessorWPF.Backend
{
    public class BuilderB
    {
        private readonly List<MeshElement> _meshElements = new List<MeshElement>();
        public List<double> CoordinatesOfPoints { get; set; }
        public List<int> NumberOfPoints { get; set; }
        public List<Point3D> Points3D { get; set; }


        public BuilderB()
        {
            CoordinatesOfPoints = new List<double>();
            NumberOfPoints = new List<int>();
            Points3D = new List<Point3D>();
        }

        /// <summary>
        ///     Получение номеров точек и разложение их на элементы сетки.
        /// </summary>
        private void GetMeshIndex()
        {
            var count = NumberOfPoints.Count/27;
            for (var i = 0; i < count; i++)
            {
                var meshElement = new MeshElement();
                for (var j = 0; j < 27; j++)
                {
                    meshElement.MeshIndex[j] = NumberOfPoints[i*27 + j];
                }
                _meshElements.Add(meshElement);
            }
        }

        /// <summary>
        ///     Преобразование списка координат сетки в точки.
        /// </summary>
        private void GetMeshPoints()
        {
            for (int i = 0, j = 1; i < CoordinatesOfPoints.Count - 2; i += 3, j++)
            {
                var x = CoordinatesOfPoints[i];
                var y = CoordinatesOfPoints[i + 1];
                var z = CoordinatesOfPoints[i + 2];

                var point = new Point3D(x, y, z) {Id = j};
                Points3D.Add(point);
            }
        }

        /// <summary>
        ///     Запись координат точек элемента по номеру.
        /// </summary>
        public void Numerate()
        {
            var task = new Task(() =>
            {
                var watch = new Stopwatch();

                watch.Start();
                Parallel.ForEach(_meshElements, element =>
                {
                    for (var i = 0; i < 27; i++)
                    {
                        foreach (var point3D in Points3D)
                        {
                            if (element.MeshIndex[i] == point3D.Id)
                                element.MeshPoints[i] = point3D;
                        }
                    }
                });

                watch.Stop();
                MessageBox.Show("Время подсчета: " + watch.ElapsedMilliseconds);
                // Разбиение 1 элемента на 8
                Generator.GetCurrentElements(_meshElements);
            });
           task.Start();
        }

        /// <summary>
        ///     Считывание бинарного файла с координатами узлов сетки.
        /// </summary>
        /// <param name="path">путь к файлу</param>
        public void ReadPointsB(string path)
        {
            try
            {
                using (var reader = new BinaryReader(File.Open(path, FileMode.Open), Encoding.ASCII))
                {
                    while (reader.PeekChar() > -1)
                    {
                        CoordinatesOfPoints.Add(reader.ReadDouble());
                    }
                    reader.Close();
                }
                GetMeshPoints();

                Numerate();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        /// <summary>
        ///     Считывание бинарного файла с массивом целочисленных номеров, обозначающих элементы (по 27 номеров).
        /// </summary>
        /// <param name="path">путь к файлу</param>
        public void ReadIndexB(string path)
        {
            try
            {
                using (var reader = new BinaryReader(File.Open(path, FileMode.Open), Encoding.ASCII))
                {
                    while (reader.PeekChar() > -1)
                    {
                        NumberOfPoints.Add(reader.ReadInt32());
                    }

                    reader.Close();
                }
                GetMeshIndex();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }
    }
}