﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.SceneGraph;
using SharpGL.WPF;

namespace PostprocessorWPF.Backend
{
    public class Drawer
    {
        public Drawer()
        {
            Control = new OpenGLControl();
            Control.OpenGLInitialized += OnInitialized;
            Control.OpenGLDraw += OnDraw;
            Control.MouseMove += OnMouseMove;
            Control.MouseDown += OnMouseDown;
            Control.MouseWheel += OnMouseWheel;
            Control.RenderContextType = RenderContextType.FBO;

            Builder = new Builder();
            ElementDrawer = new ElementDrawer();
            Camera = new Camera();
            Mouse = new Mouse();
        }

        public OpenGLControl Control { get; }

        public Builder Builder { get; }
        public ElementDrawer ElementDrawer { get; }

        public Camera Camera { get; }

        public Mouse Mouse { get; }

        public bool IsOrtho { get; set; }

        public int Step { get; set; } = 5;

        private Color BackgroundColor => Colors.DarkGray;

        private void OnInitialized(object sender, OpenGLEventArgs args)
        {
            var gl = args.OpenGL;
            gl.ClearColor(BackgroundColor.ScR, BackgroundColor.ScG, BackgroundColor.ScB, BackgroundColor.ScA);
            gl.Enable(OpenGL.GL_DEPTH_TEST);

            gl.ClearDepth(1.0f);
        }


        private void OnDraw(object sender, OpenGLEventArgs args)
        {
            var gl = args.OpenGL;

            gl.MatrixMode(MatrixMode.Projection);
            gl.LoadIdentity();
            if (IsOrtho)
            {
                gl.Ortho(-Camera.Position.Length, Camera.Position.Length, -Camera.Position.Length,
                    Camera.Position.Length,
                    0.0, 100.0);
            }
            else
            {
                gl.Perspective(60.0, (double) gl.RenderContextProvider.Width/gl.RenderContextProvider.Height,
                    0.1, 100.0);
            }
            gl.MatrixMode(MatrixMode.Modelview);

            gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT);
            gl.LoadIdentity();
            gl.LookAt(
                Camera.Position.X, Camera.Position.Y, Camera.Position.Z,
                Camera.Target.X, Camera.Target.Y, Camera.Target.Z,
                Camera.Up.X, Camera.Up.Y, Camera.Up.Z);


            
            Builder.Draw(gl);

            // Отрисовка криволинейных сеток
            ElementDrawer.DrawAll(gl);

            DrawAxes(gl);
            DrawNumbers(gl);

            gl.Flush();
        }

        /// <summary>
        ///     Отрисовка осей декартовой системы координат.
        /// </summary>
        /// <param name="gl"></param>
        private static void DrawAxes(OpenGL gl)
        {
            gl.LineWidth(4);
            gl.Begin(BeginMode.Lines);
            gl.Color(1.0, 0.0, 0.0);
            gl.Vertex(0, 0, 0);
            gl.Vertex(200, 0, 0);
            gl.Color(0.0, 1.0, 0.0);
            gl.Vertex(0, 0, 0);
            gl.Vertex(0, 200, 0);
            gl.Color(0.0, 0.0, 1.0);
            gl.Vertex(0, 0, 0);
            gl.Vertex(0, 0, 200);
            gl.End();
        }

        /// <summary>
        ///     Отрисовка подписей к линиям осей Ox,Oy,Oz.
        /// </summary>
        /// <param name="gl"></param>
        private void DrawNumbers(OpenGL gl)
        {
            const int count = 10;
            const int length = 1;
            // X coord
            for (var i = 0; i < count; i++)
            {
                gl.Color(0.8, 0, 0);
                gl.Begin(BeginMode.Lines);
                gl.Vertex(Step*i, 0, 0);
                gl.Vertex(Step*i, 0, -length);
                gl.End();
                gl.PushMatrix();
                gl.Translate(Step*i + 0.2, 0, -length);
                gl.Rotate(90, 1, 0, 0);
                gl.DrawText3D("forX", 10, 4, 0.03f, (Step*i).ToString());
                gl.PopMatrix();
            }
            // Y coord
            for (var i = 0; i < count; i++)
            {
                gl.Color(0, 0.8, 0);
                gl.Begin(BeginMode.Lines);
                gl.Vertex(0, Step*i, 0);
                gl.Vertex(0, Step*i, -length);
                gl.End();
                gl.PushMatrix();
                gl.Translate(0, Step*(i + 1), -length);
                gl.Rotate(90, 1, 0, 0);
                gl.Rotate(90, 0, -1, 0);
                gl.DrawText3D("forY", 10, 4, 0.03f, (Step*(i + 1)).ToString());
                gl.PopMatrix();
            }
            // Z coord
            for (var i = 0; i < count; i++)
            {
                gl.Color(0, 0, 0.8);
                gl.Begin(BeginMode.Lines);
                gl.Vertex(0, 0, Step*i);
                gl.Vertex(-length, 0, Step*i);
                gl.End();
                gl.PushMatrix();
                gl.Translate(-length, 0, Step*(i + 1) + 0.2);
                gl.Rotate(90, 1, 0, 0);
                gl.DrawText3D("forZ", 10, 4, 0.03f, (Step*(i + 1)).ToString());
                gl.PopMatrix();
            }
        }


        private void OnMouseMove(object sender, MouseEventArgs e)
        {
            var mousePosition = e.GetPosition(Control);
            var offset = new Point(mousePosition.X - Mouse.Position.X, mousePosition.Y - Mouse.Position.Y);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Camera.HorizontalAngle += Mouse.RotateSpeed*offset.X;
                Camera.VerticalAngle += Mouse.RotateSpeed*offset.Y;
            }
            if (e.RightButton == MouseButtonState.Pressed)
            {
                Camera.Position -= Mouse.MoveSpeed*Camera.Right*offset.X;
                Camera.Position += Mouse.MoveSpeed*Camera.Up*offset.Y;
            }
            Mouse.Position = mousePosition;
        }

        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                Mouse.Position = e.GetPosition(Control);
            }
        }

        private void OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            Camera.Position += Mouse.ZoomSpeed*Camera.Direction*e.Delta;
        }

        /// <summary>
        ///     Приближение камеры.
        /// </summary>
        public void ZoomIn()
        {
            Camera.Position += Mouse.ZoomSpeed*Camera.Direction*100;
        }

        /// <summary>
        ///     Отдаление камеры.
        /// </summary>
        public void ZoomOut()
        {
            Camera.Position -= Mouse.ZoomSpeed*Camera.Direction*100;
        }

        /// <summary>
        ///     Поворот камеры по оси Х
        /// </summary>
        /// <param name="isPlus">true - положительное направление, иначе false</param>
        public void CameraRotateX(bool isPlus)
        {
            if (isPlus)
                Camera.HorizontalAngle += Mouse.RotateSpeed*100;
            else
                Camera.HorizontalAngle -= Mouse.RotateSpeed*100;
        }

        /// <summary>
        ///     Поворот камеры по оси Y
        /// </summary>
        /// <param name="isPlus">true - положительное направление, иначе false</param>
        public void CameraRotateY(bool isPlus)
        {
            if (isPlus)
                Camera.VerticalAngle += Mouse.RotateSpeed*50;
            else
                Camera.VerticalAngle -= Mouse.RotateSpeed*50;
        }

        /// <summary>
        ///     Перемещение камеры по оси Х
        /// </summary>
        /// <param name="isPlus">true - положительное направление, иначе false</param>
        public void CameraMooveX(bool isPlus)
        {
            if (isPlus)
                Camera.Position -= Mouse.MoveSpeed*Camera.Right*50;
            else Camera.Position += Mouse.MoveSpeed*Camera.Right*50;
        }

        /// <summary>
        ///     Перемещение камеры по оси Y
        /// </summary>
        /// <param name="isPlus">true - положительное направление, иначе false</param>
        public void CameraMooveY(bool isPlus)
        {
            if (isPlus)
                Camera.Position += Mouse.MoveSpeed*Camera.Up*50;
            else Camera.Position -= Mouse.MoveSpeed*Camera.Up*50;
        }

        /// <summary>
        ///     Перемещение камеры на указанную позизию.
        /// </summary>
        /// <param name="position"></param>
        public void MoveAt(Vector3D position)
        {
            Camera.Position = position;
        }

        /// <summary>
        ///     Вращение камеры на указаную позицию.
        /// </summary>
        /// <param name="target"></param>
        public void LookAt(Vector3D target)
        {
            Camera.LookAt(target);
        }
    }
}