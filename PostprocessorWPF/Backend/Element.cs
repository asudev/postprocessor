﻿using System.Windows.Media;
using SharpGL;
using SharpGL.Enumerations;

namespace PostprocessorWPF.Backend
{
    public enum DrawMode
    {
        SolidWireframe,
        Wireframe,
        Solid
    }

    public class Element
    {
        public Element()
        {
            Points = new Point3D[8];
        }

        public int Material { get; set; }

        public DrawMode Mode { private get; set; }

        public Point3D[] Points { get; }

        public Sector Sector { get; set; }

        private Color FirstMaterialColor => Colors.LightCoral;

        private Color SecondMaterialColor => Colors.CornflowerBlue;

        private Color ThirdMaterialColor => Colors.LightGreen;

        private Color DefaultMaterialColor => Colors.Goldenrod;

        private Color EdgeColor => Colors.Black;

        public bool IsInBounds(Bounds bounds)
        {
            if (Points[0].X < bounds.X1 || Points[7].X > bounds.X2) return false;
            if (Points[0].Y < bounds.Y1 || Points[7].Y > bounds.Y2) return false;
            if (Points[0].Z < bounds.Z1 || Points[7].Z > bounds.Z2) return false;
            return true;
        }

        public void Draw(OpenGL gl)
        {
            if (Material == 0) return;

            if (Mode == DrawMode.SolidWireframe || Mode == DrawMode.Solid)
            {
                switch (Material)
                {
                    case 1:
                        gl.Color(FirstMaterialColor.ScR, FirstMaterialColor.ScG, FirstMaterialColor.ScB);
                        break;
                    case 2:
                        gl.Color(SecondMaterialColor.ScR, SecondMaterialColor.ScG, SecondMaterialColor.ScB);
                        break;
                    case 3:
                        gl.Color(ThirdMaterialColor.ScR, ThirdMaterialColor.ScG, ThirdMaterialColor.ScB);
                        break;
                    default:
                        gl.Color(DefaultMaterialColor.ScR, DefaultMaterialColor.ScG, DefaultMaterialColor.ScB);
                        break;
                }
                // Bottom.
                gl.Begin(BeginMode.Polygon);
                gl.Vertex(Points[0].X, Points[0].Y, Points[0].Z);
                gl.Vertex(Points[2].X, Points[2].Y, Points[2].Z);
                gl.Vertex(Points[3].X, Points[3].Y, Points[3].Z);
                gl.Vertex(Points[1].X, Points[1].Y, Points[1].Z);
                gl.End();

                // Left.
                gl.Begin(BeginMode.Polygon);
                gl.Vertex(Points[0].X, Points[0].Y, Points[0].Z);
                gl.Vertex(Points[4].X, Points[4].Y, Points[4].Z);
                gl.Vertex(Points[6].X, Points[6].Y, Points[6].Z);
                gl.Vertex(Points[2].X, Points[2].Y, Points[2].Z);
                gl.End();

                // Right.
                gl.Begin(BeginMode.Polygon);
                gl.Vertex(Points[1].X, Points[1].Y, Points[1].Z);
                gl.Vertex(Points[3].X, Points[3].Y, Points[3].Z);
                gl.Vertex(Points[7].X, Points[7].Y, Points[7].Z);
                gl.Vertex(Points[5].X, Points[5].Y, Points[5].Z);
                gl.End();

                // Top.
                gl.Begin(BeginMode.Polygon);
                gl.Vertex(Points[4].X, Points[4].Y, Points[4].Z);
                gl.Vertex(Points[5].X, Points[5].Y, Points[5].Z);
                gl.Vertex(Points[7].X, Points[7].Y, Points[7].Z);
                gl.Vertex(Points[6].X, Points[6].Y, Points[6].Z);
                gl.End();

                // Front.
                gl.Begin(BeginMode.Polygon);
                gl.Vertex(Points[0].X, Points[0].Y, Points[0].Z);
                gl.Vertex(Points[1].X, Points[1].Y, Points[1].Z);
                gl.Vertex(Points[5].X, Points[5].Y, Points[5].Z);
                gl.Vertex(Points[4].X, Points[4].Y, Points[4].Z);
                gl.End();

                // Back.
                gl.Begin(BeginMode.Polygon);
                gl.Vertex(Points[3].X, Points[3].Y, Points[3].Z);
                gl.Vertex(Points[2].X, Points[2].Y, Points[2].Z);
                gl.Vertex(Points[6].X, Points[6].Y, Points[6].Z);
                gl.Vertex(Points[7].X, Points[7].Y, Points[7].Z);
                gl.End();
            }

            if (Mode == DrawMode.SolidWireframe || Mode == DrawMode.Wireframe)
            {
                gl.Color(EdgeColor.ScR, EdgeColor.ScG, EdgeColor.ScB);
                gl.LineWidth(3);
                // 

                // Bottom.
                gl.Begin(BeginMode.LineLoop);
                gl.Vertex(Points[0].X, Points[0].Y, Points[0].Z);
                gl.Vertex(Points[2].X, Points[2].Y, Points[2].Z);
                gl.Vertex(Points[3].X, Points[3].Y, Points[3].Z);
                gl.Vertex(Points[1].X, Points[1].Y, Points[1].Z);
                gl.End();

                // Left.
                gl.Begin(BeginMode.LineLoop);
                gl.Vertex(Points[0].X, Points[0].Y, Points[0].Z);
                gl.Vertex(Points[4].X, Points[4].Y, Points[4].Z);
                gl.Vertex(Points[6].X, Points[6].Y, Points[6].Z);
                gl.Vertex(Points[2].X, Points[2].Y, Points[2].Z);
                gl.End();

                // Right.
                gl.Begin(BeginMode.LineLoop);
                gl.Vertex(Points[1].X, Points[1].Y, Points[1].Z);
                gl.Vertex(Points[3].X, Points[3].Y, Points[3].Z);
                gl.Vertex(Points[7].X, Points[7].Y, Points[7].Z);
                gl.Vertex(Points[5].X, Points[5].Y, Points[5].Z);
                gl.End();

                // Top.
                gl.Begin(BeginMode.LineLoop);
                gl.Vertex(Points[4].X, Points[4].Y, Points[4].Z);
                gl.Vertex(Points[5].X, Points[5].Y, Points[5].Z);
                gl.Vertex(Points[7].X, Points[7].Y, Points[7].Z);
                gl.Vertex(Points[6].X, Points[6].Y, Points[6].Z);
                gl.End();

                // Front.
                gl.Begin(BeginMode.LineLoop);
                gl.Vertex(Points[0].X, Points[0].Y, Points[0].Z);
                gl.Vertex(Points[1].X, Points[1].Y, Points[1].Z);
                gl.Vertex(Points[5].X, Points[5].Y, Points[5].Z);
                gl.Vertex(Points[4].X, Points[4].Y, Points[4].Z);
                gl.End();

                // Back.
                gl.Begin(BeginMode.LineLoop);
                gl.Vertex(Points[3].X, Points[3].Y, Points[3].Z);
                gl.Vertex(Points[2].X, Points[2].Y, Points[2].Z);
                gl.Vertex(Points[6].X, Points[6].Y, Points[6].Z);
                gl.Vertex(Points[7].X, Points[7].Y, Points[7].Z);
                gl.End();
            }
        }
    }
}