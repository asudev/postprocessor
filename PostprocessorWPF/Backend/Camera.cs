﻿using System;
using System.Windows.Media.Media3D;

namespace PostprocessorWPF.Backend
{
    public class Camera
    {
        public Vector3D Position { get; set; } = new Vector3D(5, -5, 5);

        public double HorizontalAngle { get; set; }

        public double VerticalAngle { get; set; }

        public Vector3D Direction => new Vector3D(
            Math.Cos(VerticalAngle) * Math.Cos(HorizontalAngle),
            Math.Cos(VerticalAngle) * Math.Sin(HorizontalAngle),
            Math.Sin(VerticalAngle));

        public Vector3D Right => new Vector3D(
            Math.Cos(HorizontalAngle - Math.PI / 2.0),
            Math.Sin(HorizontalAngle - Math.PI / 2.0),
            0);

        public Vector3D Up => Vector3D.CrossProduct(Right, Direction);

        public Vector3D Target => Position + Direction;

        public void LookAt(Vector3D target)
        {
            var direction = target - Position;
            direction.Normalize();
            HorizontalAngle = Math.Atan2(direction.Y, direction.X);
            VerticalAngle = Math.Asin(direction.Z);
        }
    }
}