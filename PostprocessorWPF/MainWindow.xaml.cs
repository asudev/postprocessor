﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using Microsoft.Win32;
using PostprocessorWPF.Backend;

namespace PostprocessorWPF
{
    public partial class MainWindow
    {
        private readonly BuilderB _builderB = new BuilderB();
        private bool _materialIsVisible1 = true;
        private bool _materialIsVisible2 = true;
        private bool _materialIsVisible3 = true;
        private bool _oblastInclude = true;
        private int _triger;

        public MainWindow()
        {
            InitializeComponent();
            MenuAnimation(-241);
            DrawGrid.Children.Add(Drawer.Control);

            Builder.IsInclude = _oblastInclude;
            Drawer.LookAt(new Vector3D(0, 0, 0));
            StepTextBlock.Text = Drawer.Step.ToString();

            CameraPositionView();

            AddHotKeys();
        }

        private Drawer Drawer { get; } = new Drawer();
        private Builder Builder => Drawer.Builder;

        private void AddHotKeys()
        {
            try
            {
                var openGrid = new RoutedCommand();
                openGrid.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(openGrid, OpenGrid_event_handler));

                var generateGrid = new RoutedCommand();
                generateGrid.InputGestures.Add(new KeyGesture(Key.G, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(generateGrid, GenerateGrid_event_handler));

                var viewChange = new RoutedCommand();
                viewChange.InputGestures.Add(new KeyGesture(Key.V, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(viewChange, ViewChange_event_handler));

                var settingsCommand = new RoutedCommand();
                settingsCommand.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(settingsCommand, OpenSettings_event_handler));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void OpenGrid_event_handler(object sender, ExecutedRoutedEventArgs e)
        {
            //OpenObjectDialog();
            OpenMeshDialog();
        }

        private void GenerateGrid_event_handler(object sender, RoutedEventArgs e)
        {
            OpenMeshDialog();
            //OpenMeshDialog();
            //OpenDataDialog();
        }

        private void OpenSettings_event_handler(object sender, RoutedEventArgs e)
        {
            SettingsView(true);
        }

        private void ViewChange_event_handler(object sender, RoutedEventArgs e)
        {
            Drawer.IsOrtho = !Drawer.IsOrtho;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            // camera hotkeys
            switch (e.Key)
            {
                case Key.Q:
                    Drawer.CameraRotateX(true);
                    break;
                case Key.E:
                    Drawer.CameraRotateX(false);
                    break;
                case Key.R:
                    Drawer.CameraRotateY(true);
                    break;
                case Key.T:
                    Drawer.CameraRotateY(false);
                    break;
                case Key.O:
                    Drawer.ZoomIn();
                    CameraPositionView();
                    break;
                case Key.P:
                    Drawer.ZoomOut();
                    CameraPositionView();
                    break;
                case Key.A:
                    Drawer.CameraMooveX(true);
                    CameraPositionView();
                    break;
                case Key.D:
                    Drawer.CameraMooveX(false);
                    CameraPositionView();
                    break;
                case Key.W:
                    Drawer.CameraMooveY(true);
                    CameraPositionView();
                    break;
                case Key.S:
                    Drawer.CameraMooveY(false);
                    CameraPositionView();
                    break;
            }
        }

        private void BoundButton_OnClick(object sender, RoutedEventArgs e)
        {
            var x1Text = BoundX1TextBox.Text;
            var y1Text = BoundY1TextBox.Text;
            var z1Text = BoundZ1TextBox.Text;
            var x2Text = BoundX2TextBox.Text;
            var y2Text = BoundY2TextBox.Text;
            var z2Text = BoundZ2TextBox.Text;

            try
            {
                if (!string.IsNullOrWhiteSpace(x1Text))
                {
                    Builder.Bounds.X1 = Convert.ToDouble(x1Text);
                }
                if (!string.IsNullOrWhiteSpace(y1Text))
                {
                    Builder.Bounds.Y1 = Convert.ToDouble(y1Text);
                }
                if (!string.IsNullOrWhiteSpace(z1Text))
                {
                    Builder.Bounds.Z1 = Convert.ToDouble(z1Text);
                }
                if (!string.IsNullOrWhiteSpace(x2Text))
                {
                    Builder.Bounds.X2 = Convert.ToDouble(x2Text);
                }
                if (!string.IsNullOrWhiteSpace(y2Text))
                {
                    Builder.Bounds.Y2 = Convert.ToDouble(y2Text);
                }
                if (!string.IsNullOrWhiteSpace(z2Text))
                {
                    Builder.Bounds.Z2 = Convert.ToDouble(z2Text);
                }

                Builder.IsInclude = _oblastInclude;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void GridCube_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            if (grid != null)
            {
                var tag = grid.Tag.ToString();

                switch (tag)
                {
                    case "SolidWireframeIcon":
                        Drawer.Builder.SetMode(DrawMode.SolidWireframe);
                        Views.Source = new BitmapImage(new Uri("Assets/iconCube2.png", UriKind.Relative));
                        break;
                    case "SolidIcon":
                        Drawer.Builder.SetMode(DrawMode.Solid);
                        Views.Source = new BitmapImage(new Uri("Assets/iconCube1.png", UriKind.Relative));
                        break;
                    case "WireframeIcon":
                        Drawer.Builder.SetMode(DrawMode.Wireframe);
                        Views.Source = new BitmapImage(new Uri("Assets/iconCube3.png", UriKind.Relative));
                        break;
                }
            }

            MenuItemAnim(sender, 255, 255, 255);
        }

        private void SettingsView(bool isShown)
        {
            if (isShown)
            {
                MenuImage.Source = new BitmapImage(new Uri("Assets/iconMenu2.png", UriKind.Relative));
                SettingsGrid.Visibility = Visibility.Visible;
                Icon1.Visibility = Visibility.Collapsed;
                Icon2.Visibility = Visibility.Collapsed;
                Icon3.Visibility = Visibility.Collapsed;
                Icon4.Visibility = Visibility.Collapsed;
                Icon5.Visibility = Visibility.Collapsed;
                Icon7.Visibility = Visibility.Collapsed;
            }
            else
            {
                MenuImage.Source = new BitmapImage(new Uri("Assets/iconMenu1.png", UriKind.Relative));
                SettingsGrid.Visibility = Visibility.Collapsed;
                Icon1.Visibility = Visibility.Visible;
                Icon2.Visibility = Visibility.Visible;
                Icon3.Visibility = Visibility.Visible;
                Icon4.Visibility = Visibility.Visible;
                Icon5.Visibility = Visibility.Visible;
                Icon7.Visibility = Visibility.Visible;
            }
        }

        private void ViewChoice()
        {
            Drawer.IsOrtho = !Drawer.IsOrtho;
            ProjectionImage.Source = Drawer.IsOrtho
                ? new BitmapImage(new Uri("Assets/icon2.png", UriKind.Relative))
                : new BitmapImage(new Uri("Assets/icon3.png", UriKind.Relative));

            MainViewText.Text = Drawer.IsOrtho ? "Ортогональный" : "Перспектива";
            SubViewText.Text = Drawer.IsOrtho ? "Перспектива" : "Ортогональный";
        }

        private void OblastChoice()
        {
            _oblastInclude = !_oblastInclude;

            Oblast.Source = _oblastInclude
                ? new BitmapImage(new Uri("Assets/iconPlus.png", UriKind.Relative))
                : new BitmapImage(new Uri("Assets/iconMinus.png", UriKind.Relative));

            MainOblastText.Text = _oblastInclude ? "Включение области" : "Исключение области";
            SubOblastText.Text = _oblastInclude ? "Исключение области" : "Включение области";
        }

        private void SetupSettings()
        {
            Drawer.Mouse.MoveSpeed = MoveSpeed.Value/1000;
            Drawer.Mouse.ZoomSpeed = ZoomSpeed.Value/1000;

            if (StepTextBlock.Text != "")
                Drawer.Step = Convert.ToInt32(StepTextBlock.Text);
        }

        private void FileChooser(string path1, string path2)
        {
            if (Path.GetExtension(path1) == ".dat" && Path.GetExtension(path2) == ".dat")
            {
                _builderB.ReadIndexB(path1);
                _builderB.ReadPointsB(path2);
            }
            else if (Path.GetExtension(path1) == ".txt" && Path.GetExtension(path2) == ".txt")
            {
                Builder.ReadPoints2(path1);
                Builder.ReadElements(path2);
                Builder.IsFirst = true;
            }
            else
            {
                MessageBox.Show("Вы должны выбрать оба файла с одинаковым типом: .txt либо .dat",
                    "Ошибка загрузки данных");
            }
        }

        private void OpenMeshDialog()
        {
            var openFileDialog = new OpenFileDialog
            {
                Title = @"Выберите файл содержащий номера узлов (int)",
                Filter = @"Бинарные файлы|*.dat|Текстовые файлы|*.txt",
                CheckFileExists = true,
                AddExtension = true,
                CheckPathExists = true,
                FileName = "nver_curv"
            };
            if (openFileDialog.ShowDialog() != true) return;
            var indexes = openFileDialog.FileName;

            openFileDialog = new OpenFileDialog
            {
                Title = @"Выберите файл содержащий координаты узлов (double)",
                Filter = @"Бинарные файлы|*.dat|Текстовые файлы|*.txt",
                CheckFileExists = true,
                AddExtension = true,
                CheckPathExists = true,
                FileName = "xyz_curv"
            };
            if (openFileDialog.ShowDialog() != true) return;
            var coordinates = openFileDialog.FileName;

            try
            {
                FileChooser(indexes, coordinates);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        private void OpenDataDialog()
        {
            var openFileDialog = new OpenFileDialog
            {
                Title = @"Выберите файл Points",
                Filter = @"Текстовые файлы|*.dat",
                CheckFileExists = true,
                AddExtension = true,
                CheckPathExists = true,
                FileName = "Points"
            };
            if (openFileDialog.ShowDialog() != true) return;
            var pointsPath = openFileDialog.FileName;

            //openFileDialog = new OpenFileDialog
            //{
            //    Title = @"Выберите файл Sectors",
            //    Filter = @"Текстовые файлы|*.txt",
            //    CheckFileExists = true,
            //    AddExtension = true,
            //    CheckPathExists = true,
            //    FileName = "Sectors"
            //};
            //if (openFileDialog.ShowDialog() != true) return;
            //var sectorsPath = openFileDialog.FileName;

            //openFileDialog = new OpenFileDialog
            //{
            //    Title = @"Выберите файл Partitions",
            //    Filter = @"Текстовые файлы|*.txt",
            //    CheckFileExists = true,
            //    AddExtension = true,
            //    CheckPathExists = true,
            //    FileName = "Partition"
            //};
            //if (openFileDialog.ShowDialog() != true) return;
            //var partitionsPath = openFileDialog.FileName;

            try
            {
                //var mesh = new Mesh();
                //mesh.InputArea(pointsPath, sectorsPath, partitionsPath);
                //mesh.Generate();
                //mesh.WritePoints("../../Data/MeshPoints.txt");
                //mesh.WriteElements("../../Data/MeshElements.txt");
                // WriteDefaultValues(pointsPath);
                //Builder.ReadBytePointsInt(pointsPath);
                //Builder.ReadByteDouble(pointsPath);
                //Builder.ReadElements("../../Data/MeshElements.txt");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var s = sender as Slider;
            if (s == null) return;

            if (s.Tag.Equals("1"))
                SliderText1.Text = Convert.ToInt32(s.Value).ToString();
            else if (s.Tag.Equals("2"))
                SliderText2.Text = Convert.ToInt32(s.Value).ToString();
        }

        private void MoveAtButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var x = Convert.ToDouble(PositionXTextBox.Text);
                var y = Convert.ToDouble(PositionYTextBox.Text);
                var z = Convert.ToDouble(PositionZTextBox.Text);
                Drawer.MoveAt(new Vector3D(x, y, z));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
            }
        }

        private void LookAtButton_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var x = Convert.ToDouble(TargetXTextBox.Text);
                var y = Convert.ToDouble(TargetYTextBox.Text);
                var z = Convert.ToDouble(TargetZTextBox.Text);
                Drawer.LookAt(new Vector3D(x, y, z));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка!");
            }
        }

        private void FPS_check_Click(object sender, RoutedEventArgs e)
        {
            if (FPS_check.IsChecked == true)
            {
                Drawer.Control.DrawFPS = true;
            }
            else
            {
                Drawer.Control.DrawFPS = false;
            }
        }

        private void Material1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_materialIsVisible1)
            {
                Builder.MaterialsToDraw.Remove(1);
                _materialIsVisible1 = !_materialIsVisible1;
            }
            else
            {
                Builder.MaterialsToDraw.Add(1);
                _materialIsVisible1 = !_materialIsVisible1;
            }
        }

        private void Material2_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_materialIsVisible2)
            {
                Builder.MaterialsToDraw.Remove(2);
                _materialIsVisible2 = !_materialIsVisible2;
            }
            else
            {
                Builder.MaterialsToDraw.Add(2);
                _materialIsVisible2 = !_materialIsVisible2;
            }
        }

        private void Material3_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (_materialIsVisible3)
            {
                Builder.MaterialsToDraw.Remove(3);
                _materialIsVisible3 = !_materialIsVisible3;
            }
            else
            {
                Builder.MaterialsToDraw.Add(3);
                _materialIsVisible3 = !_materialIsVisible3;
            }
        }

        private void StepTextBlock_TextChanged(object sender, TextChangedEventArgs e)
        {
            try
            {
                var step = Convert.ToInt32(StepTextBlock.Text);
            }
            catch (Exception ex)
            {
                if (StepTextBlock.Text != "")
                    MessageBox.Show(ex.Message);
            }
        }

        private void DrawGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            CameraPositionView();
        }

        private void CameraPositionView()
        {
            PositionXTextBox.Text = Drawer.Camera.Position.X.ToString("F1");
            PositionYTextBox.Text = Drawer.Camera.Position.Y.ToString("F1");
            PositionZTextBox.Text = Drawer.Camera.Position.Z.ToString("F1");
        }

        #region Header

        private void Header_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            HeaderAnim(255, 57, 75, 91);

            if (SettingsGrid.Visibility == Visibility.Visible)
            {
                SettingsView(false);
            }
            else
            {
                if (_triger == 0)
                {
                    MenuAnimation(0);
                    _triger = 1;
                }
                else
                {
                    MenuAnimation(-241);
                    _triger = 0;
                }
            }
        }

        private void Header_MouseEnter(object sender, MouseEventArgs e)
        {
            HeaderAnim(255, 57, 75, 91);
        }

        private void Header_MouseLeave(object sender, MouseEventArgs e)
        {
            HeaderAnim(255, 40, 55, 71);
        }

        private void Header_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            HeaderAnim(255, 20, 33, 45);
        }

        #endregion Header

        #region BackButton

        private void Back_MouseEnter(object sender, MouseEventArgs e)
        {
            MenuItemAnim(sender, 160, 160, 160);
        }

        private void Back_MouseLeave(object sender, MouseEventArgs e)
        {
            MenuItemAnim(sender, 191, 191, 191);
        }

        private void Back_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MenuItemAnim(sender, 40, 55, 71);
        }

        #endregion BackButton

        #region Grid

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            MenuItemAnim(sender, 255, 255, 255);
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            MenuItemAnim(sender, 242, 242, 242);
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            MenuItemAnim(sender, 200, 200, 200);
        }

        private void Grid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            if (grid != null)
            {
                var tag = grid.Tag.ToString();

                MenuItemAnim(sender, 255, 255, 255);

                switch (tag)
                {
                    case "1":
                    case "icon1":
                        OpenMeshDialog();
                        MenuItemAnim(sender, 242, 242, 242);
                        break;
                    case "2":
                    case "icon2":
                        //OpenMeshDialog();
                        OpenDataDialog();
                        MenuItemAnim(sender, 242, 242, 242);
                        break;
                    case "3":
                    case "icon3":
                        ViewChoice();
                        break;
                    case "5":
                    case "icon5":
                        OblastChoice();
                        break;
                    case "7":
                    case "icon7":
                        SettingsView(true);
                        break;
                    case "8":
                        SettingsView(false);
                        SetupSettings();
                        break;
                }
            }
        }

        #endregion Grid

        #region Controls

        private void Controls_MouseEnter(object sender, MouseEventArgs e)
        {
            ControlsAnim(sender, 255);
        }

        private void Controls_MouseLeave(object sender, MouseEventArgs e)
        {
            ControlsAnim(sender, 178);
        }

        private void Controls_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            if (grid == null) return;

            switch (grid.Tag.ToString())
            {
                case "yL2":
                    Drawer.CameraMooveY(true);
                    CameraPositionView();
                    break;
                case "yR2":
                    Drawer.CameraMooveY(false);
                    CameraPositionView();
                    break;
                case "xL2":
                    Drawer.CameraMooveX(true);
                    CameraPositionView();
                    break;
                case "xR2":
                    Drawer.CameraMooveX(false);
                    CameraPositionView();
                    break;
                case "xL":
                    Drawer.CameraRotateX(true);
                    break;
                case "xR":
                    Drawer.CameraRotateX(false);
                    break;
                case "yL":
                    Drawer.CameraRotateY(true);
                    break;
                case "yR":
                    Drawer.CameraRotateY(false);
                    break;
                case "z+":
                    Drawer.ZoomIn();
                    break;
                case "z-":
                    Drawer.ZoomOut();
                    break;
                default:
                    break;
            }

            ControlsAnim(sender, 50);
        }

        private void Controls_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var grid = sender as Grid;
            if (grid == null) return;

            if (grid.Tag.Equals("center"))
                try
                {
                    Drawer.LookAt(new Vector3D(0, 0, 0));
                }
                catch (Exception exception)
                {
                    Debug.WriteLine(exception);
                }

            ControlsAnim(sender, 255);
        }

        #endregion Controls

        #region Animation

        private void MenuItemAnim(object sender, byte toR, byte toG, byte toB)
        {
            var storyboard = new Storyboard();
            var colorAnim = new ColorAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(100)),
                To = Color.FromArgb(255, toR, toG, toB)
            };

            Storyboard.SetTargetProperty(colorAnim,
                new PropertyPath("(Panel.Background).(SolidColorBrush.Color)"));
            Storyboard.SetTarget(colorAnim, sender as Grid);

            try
            {
                var colorAnim2 = new ColorAnimation
                {
                    Duration = new Duration(TimeSpan.FromMilliseconds(100)),
                    To = Color.FromArgb(255, toR, toG, toB)
                };

                var o = sender as Grid;
                if (o != null)
                {
                    switch (o.Tag.ToString())
                    {
                        case "1":
                            Storyboard.SetTarget(colorAnim2, Icon1);
                            break;
                        case "2":
                            Storyboard.SetTarget(colorAnim2, Icon2);
                            break;
                        case "3":
                            Storyboard.SetTarget(colorAnim2, Icon3);
                            break;
                        case "5":
                            Storyboard.SetTarget(colorAnim2, Icon5);
                            break;
                        case "7":
                            Storyboard.SetTarget(colorAnim2, Icon7);
                            break;
                        default:
                            Storyboard.SetTarget(colorAnim2, sender as Grid);
                            break;
                    }
                }

                Storyboard.SetTargetProperty(colorAnim2,
                    new PropertyPath("(Panel.Background).(SolidColorBrush.Color)"));

                storyboard.Children.Add(colorAnim2);
            }
            catch (Exception)
            {
                // ignored
            }

            storyboard.Children.Add(colorAnim);

            storyboard.Begin();
        }

        private void HeaderAnim(byte toA, byte r, byte g, byte b)
        {
            var storyboard = new Storyboard();
            var colorAnim = new ColorAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(100)),
                To = Color.FromArgb(toA, r, g, b)
            };
            Storyboard.SetTargetProperty(colorAnim,
                new PropertyPath("(Panel.Background).(SolidColorBrush.Color)"));
            Storyboard.SetTarget(colorAnim, HeaderGrid);

            var colorAnim2 = new ColorAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(100)),
                To = Color.FromArgb(toA, r, g, b)
            };
            Storyboard.SetTargetProperty(colorAnim2,
                new PropertyPath("(Panel.Background).(SolidColorBrush.Color)"));
            Storyboard.SetTarget(colorAnim2, MenuIcon);

            storyboard.Children.Add(colorAnim);
            storyboard.Children.Add(colorAnim2);

            storyboard.Begin();
        }

        private void ControlsAnim(object sender, byte toA)
        {
            var storyboard = new Storyboard();
            var colorAnim = new ColorAnimation
            {
                Duration = new Duration(TimeSpan.FromMilliseconds(100)),
                To = Color.FromArgb(toA, 255, 255, 255)
            };
            Storyboard.SetTargetProperty(colorAnim,
                new PropertyPath("(Panel.Background).(SolidColorBrush.Color)"));
            Storyboard.SetTarget(colorAnim, sender as Grid);

            storyboard.Children.Add(colorAnim);

            storyboard.Begin();
        }

        private void MenuAnimation(int to)
        {
            var storyboard = new Storyboard();
            var doubleAnimation = new DoubleAnimation
            {
                To = to,
                Duration = new Duration(TimeSpan.FromMilliseconds(400)),
                EasingFunction = new CircleEase {EasingMode = EasingMode.EaseOut}
            };

            Storyboard.SetTargetProperty(doubleAnimation,
                new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.X)"));
            Storyboard.SetTarget(doubleAnimation, MenuGrid);

            storyboard.Children.Add(doubleAnimation);
            storyboard.Begin();
        }

        #endregion Animation
    }
}